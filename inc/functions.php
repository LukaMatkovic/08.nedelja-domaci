<?php        
        function test_input($data) {
            $data=trim($data);
            $data=stripslashes($data);
            $data=htmlspecialchars($data);
            return $data;
        }

        function checkImage($file) {
            if(exif_imagetype($file["tmp_name"])==2 || exif_imagetype($file["tmp_name"])==1) {

                if ($file["size"]< 1024*1024){
                    return true;
                }
                else {
                    return false;
                }
            }
            else {
                return false;
            }
        }
?>