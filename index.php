<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="keywords" content="PHP, form, upload, photo">
    <meta name="author" content="Luka Matkovic">

    <title>Upload</title>
    <style></style>

</head>
<body>

    <?php
        $name = $privacy = $photo = "";
        $nameErr = $privacyErr = $photoErr = "";

        if (isset($_GET['name'])) { $name = $_GET['name']; }
        if (isset($_GET['privacy'])) { $privacy = $_GET['privacy']; }
        if (isset($_GET['photo'])) { $photo = $_GET['photo']; }

        if (isset($_GET['nameErr'])) { $nameErr = $_GET['nameErr']; }
        if (isset($_GET['privacyErr'])) { $privacyErr = $_GET['privacyErr']; }
        if (isset($_GET['photoErr'])) { $photoErr = $_GET['photoErr']; }

    ?>

    <form action="myPhoto.php" method="post" enctype="multipart/form-data" name="upload">

        <label for="name">Name <span class="error"> * <?php echo $nameErr; ?></span></label><br>
        <input type="text" name="name" value="<?php echo $name; ?>"><br>
        
        <label for="privacy">Privacy <span class="error"> * <?php echo $privacyErr; ?></span><br></label>
        <input type="radio" name="privacy" <?php if (isset($privacy) && $privacy=="private") echo "checked";?> value="private">Private<br>
        <input type="radio" name="privacy" <?php if (isset($privacy) && $privacy=="public") echo "checked";?> value="public">Public<br>

        <label for="photo">Upload photo <span class="error"> * <?php echo $photoErr; ?></span></label><br>
        <input type="file" name="photo"><br>

        <input type="submit" name="submit" value="upload">
        <input type="reset" name="reset" value="cancel">

    </form>
</body>
</html>